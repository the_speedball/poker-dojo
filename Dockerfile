FROM python:2.7
COPY server_testing.py /app/
COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt
