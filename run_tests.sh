#!/bin/bash

if [ "%$(docker network ls | grep dockernet)%" = "%%" ]; then
    docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 dockernet
else
    echo ""
fi

docker build . -t dojo-tdd
docker run -it --net=dockernet dojo-tdd python /app/server_testing.py
