# DOJO #

`git clone https://gitlab.com/the_speedball/poker-dojo`

# START GAME

In order to start game make call. Each game has to have same players for the whole duration of the game.

     GET /game/{UUID}/one
     
     
     rsp = {
         'stage': 'one',
         'next_stage': 'two',
         'dealer': {
             'hand': ['2d', 'Ad', '4d', 'Qh']
         },
         'players': {
             'total':
             2,
             '_embedded': [{
                 'name': 'Fred',
                 'hand': ['2h', '5h']
             }, {
                 'name': 'Fred',
                 'hand': ['4h', '7h']
             }]
         }
     }
     
`one` is name of the stage. This will create a game for 2-10 players.

# CHECK OTHER STAGE OF GAME

     GET /game/{UUID}/three

     rsp = {
         'stage': 'three',
         'next_stage': 'four',
         'dealer': {
             'hand': ['2d', 'Ad', '4d', 'Qh', '3c']
         },
         'players': {
             'total':
             2,
             '_embedded': [{
                 'name': 'Fred',
                 'hand': ['2h', '5h']
             }, {
                 'name': 'Fred',
                 'hand': ['4h', '7h']
             }]
         }
     }
     
Allowed stages are `['one', 'two', 'three', 'four']`.
Stage four should reveal winner.

# CREATE GAME WITH NUMBER OF PLAYERS SPECIFIED

Game can be created with specified number of players. Only 2-10 players are allowed.

     GET /game/{UUID}/one?players=2

     rsp = {
         'stage': 'one',
         'next_stage': 'two',
         'dealer': {
             'hand': ['2d', 'Ad', '4d', 'Qh']
         },
         'players': {
             'total':
             2,
             '_embedded': [{
                 'name': 'Fred',
                 'hand': ['2h', '5h']
             }, {
                 'name': 'Fred',
                 'hand': ['4h', '7h']
             }]
         }
     }
  
