import uuid
import random
from itertools import chain

import requests
import pytest
from deuces import Card, Evaluator
from cerberus import Validator

# pytest -vx
GAME_STAGE_URL = 'http://192.168.0.1:8080/game/{game_num}/{stage}'

# S spades, H hearts, D diamonds, C clubs
DECK = list(
    chain.from_iterable(
        [['%s%s' % (face, color) for color in ['s', 'h', 'd', 'c']]
         for face in [2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A']]))

next_stages = {'one': 'two', 'two': 'three', 'three': 'four', 'four': ''}
dealer_hand_size = {'one': 3, 'two': 4, 'three': 5, 'four': 5}
player_schema = {
    'name': {
        'type': 'string',
        'required': True
    },
    'hand': {
        'required': True,
        'type': 'list',
        'schema': {
            'type': 'string'
        },
        'minlength': 2,
        'maxlength': 2
    }
}


def get_winner(players, dealer):
    dealer = [Card.new(c.lower()) for c in dealer]

    evaluator = Evaluator()
    # create deuces hand for each players
    scores = {
        p['name']: evaluator.evaluate(dealer,
                                      [Card.new(c.lower()) for c in p['hand']])
        for p in players
    }
    winner, _ = max(scores.items(), lambda x: x[1])
    return winner


def verify_stage(rsp, number_of_players):
    assert 2 <= number_of_players <= 10, "Only 2-10 players allowed"

    stage = rsp['stage']
    assert rsp['stage'] == stage
    assert rsp['next_stage'] == next_stages[stage]

    if stage == 'four':
        assert rsp['winner'], "Winner is missing"
    else:
        assert not rsp.get('winner'), "Winner only in stage 'four'"

    assert rsp['players']['total'] == number_of_players
    assert len(rsp['players']['_embedded']) == number_of_players

    dealer_hand = rsp['dealer']['hand']
    assert len(dealer_hand) == dealer_hand_size[stage], (
        "Dealers hand has incorrect number of cards.")

    # validate players
    v = Validator(player_schema)
    for p in rsp['players']['_embedded']:
        if not v.validate(p):
            print('errors: %s' % v.errors)
            print('player: %s' % p)
            assert False, "Errors: %s" % v.errors

    # validate cards
    all_hands = [p['hand'] for p in rsp['players']['_embedded']]

    # check number of cards
    expected_no_cards = number_of_players * 2 + dealer_hand_size[stage]
    assert len(sum(all_hands, [])) + len(dealer_hand) == expected_no_cards, (
        "Total number of cards in all"
        " hands is off. It should be 'number_of_players * 2 + dealers hand")

    validate_hands(all_hands, rsp['dealer']['hand'])

    # verify winner
    if stage == 'four':
        winner = get_winner(rsp['players']['_embedded'], rsp['dealer']['hand'])

        assert winner[0] == rsp['winner'], (
            "Winner is incorrect. "
            "Player %s should be the winner" % winner[0])


def validate_hands(hands, dealer_cards):
    """ Check if cards are unique across all hands """
    player_cards = sum(hands, [])
    # calculate number of all cards in hands
    number_of_cards = len(player_cards) + len(dealer_cards)
    # sum players and dealer cards
    all_hands = player_cards + dealer_cards
    # verify if unique
    all_unique_cards = set(all_hands)
    # number unique cards should be equal to player cards and dealer cards
    assert len(all_unique_cards) == number_of_cards

    # assert card faces
    for card in all_unique_cards:
        assert card in DECK, "Card %s is not know to me." % card


@pytest.mark.parametrize('number_of_players', [2, 3, 4, 5, 6, 7, 8, 9, 10])
def test_create_game(number_of_players):
    game_uuid = str(uuid.uuid4())
    first_stage = requests.get(
        GAME_STAGE_URL.format(game_num=game_uuid, stage='one') +
        '?players={players}'.format(players=number_of_players),
        timeout=5)
    r_first = first_stage.json()
    verify_stage(r_first, number_of_players)


def test_create_game_with_too_many_players():
    game_uuid = str(uuid.uuid4())
    first_stage = requests.get(
        GAME_STAGE_URL.format(game_num=game_uuid, stage='one') +
        '?players={players}'.format(players=12),
        timeout=5)
    assert first_stage.status_code == 400


def test_create_game_with_too_few_players():
    game_uuid = str(uuid.uuid4())
    first_stage = requests.get(
        GAME_STAGE_URL.format(game_num=game_uuid, stage='one') +
        '?players={players}'.format(players=1),
        timeout=5)
    assert first_stage.status_code == 400


@pytest.mark.parametrize('number_of_players', [2, 3, 4, 5, 6, 7, 8, 9, 10])
def test_stage_four_random_players(number_of_players):
    # start game
    # verify first_stage
    game_uuid = str(uuid.uuid4())
    first_stage = requests.get(
        GAME_STAGE_URL.format(game_num=game_uuid, stage='one') +
        '?players={players}'.format(players=number_of_players),
        timeout=5)
    r_first = first_stage.json()
    player_names = [p['name'] for p in r_first['players']['_embedded']]
    verify_stage(r_first, number_of_players)

    # check any next stage
    # verify
    next_stage_name = random.choice(['two', 'three', 'four'])
    next_stage = requests.get(
        GAME_STAGE_URL.format(game_num=game_uuid, stage=next_stage_name) +
        '?players={players}'.format(players=number_of_players),
        timeout=5)
    r_next = next_stage.json()
    assert set(player_names) == set(
        [p['name'] for p in r_next['players']['_embedded']])
    verify_stage(r_next, number_of_players)


def test_non_existing_game():
    r = requests.get(
        GAME_STAGE_URL.format(
            game_num='071dc3df-54bd-4206-a273-a3462fee1367', stage='two'),
        timeout=5)
    assert r.status_code == 404


def test_full_game():
    first_stage = requests.get(
        GAME_STAGE_URL.format(
            game_num='946c84d7-dce1-4b6f-ab2c-876a4f9c38ef', stage='one'),
        timeout=5)
    r_first = first_stage.json()
    player_names = [p['name'] for p in r_first['players']['_embedded']]
    verify_stage(r_first, len(player_names))

    second_stage = requests.get(
        GAME_STAGE_URL.format(
            game_num='946c84d7-dce1-4b6f-ab2c-876a4f9c38ef', stage='two'),
        timeout=5)
    r_second = second_stage.json()
    assert set(player_names) == set([
        p['name'] for p in r_second['players']['_embedded']
    ])
    verify_stage(r_second, len(player_names))

    third_stage = requests.get(
        GAME_STAGE_URL.format(
            game_num='946c84d7-dce1-4b6f-ab2c-876a4f9c38ef', stage='three'),
        timeout=5)
    r_third = third_stage.json()
    assert set(player_names) == set(
        [p['name'] for p in r_third['players']['_embedded']])
    verify_stage(r_third, len(player_names))

    fourth_stage = requests.get(
        GAME_STAGE_URL.format(
            game_num='946c84d7-dce1-4b6f-ab2c-876a4f9c38ef', stage='four'),
        timeout=5)
    r_fourth = fourth_stage.json()
    assert set(player_names) == set(
        [p['name'] for p in r_fourth['players']['_embedded']])
    verify_stage(r_fourth, len(player_names))


pytest.main(['-v', '/app/server_testing.py'])
